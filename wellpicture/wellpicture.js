﻿var queryUrl=appRoot+"/api/PungersParameters/SelectgyzttByOrgId";//"/api/WellPointInfo/query";

var wellConfigureImages = appRoot + "/runview.aspx"

var querydictUrl = "/api/jsp/sys/queryDict.do";//查询数据字典

var selectUrl  = appRoot + "/api/PungersParameters/SelectgyzttData";
var queryWellsUrl = appRoot + "/api/Org/GetNewWellTreeJson";//查询作业区下属井站



$(window).resize(function () {
    $("#treeDiv").css("height", document.body.clientHeight - 70);
    
});

//点击节点获取单位下属单井列表信息
var orgId="0";
var orgLevel = 0;

   
var table;

var app=angular.module('app',[]);

app.controller('wellpoints', function ($scope) {
    $("#treeDiv").css("height", document.body.clientHeight - 70);
    $("#treeDiv").css("height", $(window).height() - 70);

    $scope.level = 0;

    var onNodeClick = function (event, treeId, treeNode) {
        orgId = treeNode.id;
        orgLevel = treeNode.level;
       
        $scope.level = orgLevel;
      
        if(orgLevel< 3){
            $("#datas_wrapper").show();
            queryWells(orgId);
			
        }else{
			$("#ztPicture").attr("src",wellConfigureImages + "?filename="+treeNode.name);
            $("#datas_wrapper").hide();	
            initDatas(orgId);		
        }	 
       $scope.$apply();
    }

    $("#ztree").orgWellTree(onNodeClick, userOrgId);

    $scope.onQuery = function () {
        queryWells(orgId);
    }

var initDatas = function(orgId){
    $.get(selectUrl, { well_id: orgId}, function (ret) {
        if (0 == ret.code) {
            layer.msg(result.message, { icon: 2 });
        } else {
            $("#editForm").fill(ret.data[0]);

            $scope.$apply();
        }
    }, 'json');

}

var queryWells = function (orgId) {        
    if (orgId && table) {
        var param = {
            "keywords": $("#keywords").val(),
            "org_id": orgId
        };
        table.api().settings()[0].ajax.data = param;
        table.api().ajax.reload();
        
    } else if (orgId) {            
        table = $("#datas").dataTable({
            "language": { "url": "../../js/plugins/dataTables/Chinese.json" },
            "dom": 'rt<"bottom"iflp<"clear">>',
            "pagingType": "full_numbers",
            "searching": false,
            "ordering": false,
            "autoWidth": false,
            "processing": true,
            "serverSide": true,
            "ajax": {
                "dataSrc": "datas",
                "type": "post",
                "cache": false,
                "url": queryUrl,
                "data": function (d) {                        
                    return $.extend({}, d, {
                        "org_id": orgId
                    });
                }
            },
            "aoColumnDefs": [{
                "targets": [0],
                "data": "id",
                "render": function (data, type, row) {
                    var s = '<input type="checkbox" ';
                    if ("1" == row.rowDefault) {
                        s = s + 'disabled="disabled" ';
                    } else {
                        s = s + 'class="ids_" name="ids_" value="' + data + '" ';
                    }
                    return s + 'style="margin:0px;"/>';
                }
            }, {
                "targets": [1],
                "data": "jmjh"
            }, {
                "targets": [2],
                "data": "jx_name"
            },
            {
                "targets": [3],
                "data": "gylx_name"
            },
            {
                "targets": [4],
                "data": "zscd"
            }, {
                "targets": [5],
                "data": "zszdwj"
            }, {
                "targets": [6],
                "data": "cctysj"
            },
            {
                "targets": [7],
                "data": "tgwj"
            }, {
                "targets": [8],
                "data": "ygwj"
            }]
        });
        console.log(table);
    }
}

queryWells(orgId);

});



// $scope.wellboreUrl ='';
//       $scope.level = 0;
// 	 var onNodeClick = function (event, treeId, treeNode) {
//         orgId = treeNode.id;
//         orgLevel = treeNode.level;
       
//         $scope.level = orgLevel;
//         //$scope.$apply();
//         if(orgLevel<=3){
//             $("#datas_wrapper").show();
//              $scope.wellboreUrl =  wellConfigureImages +"?filename="+  treeNode.name;
// $("#ztPicture").attr("src",$scope.wellboreUrl);
//            //  queryWells(orgId);
//         }else{
//             $("#datas_wrapper").hide();
//         }
// console.log($scope.wellboreUrl);
// 	//$scope.$apply();
//     }

//     $("#treeDiv").css("height", $(window).height() - 70);

//     $('.form_date').datetimepicker({
//         language: 'zh-CN',
//         weekStart: 1,
//         todayBtn: 1,
//         autoclose: 1,
//         todayHighlight: 1,
//         startView: 2,
//         minView: 2,
//         forceParse: 0
//     });
//     $("#ztree").orgWellTree(onNodeClick,userOrgId);

//     $scope.onNew = function () {
//         if (orgLevel != 3) {
//             layer.msg("请选择井！", { icon: 0 });
//             return;
//         }
//         var salesDetailTable = new $.fn.dataTable.Api('#datas');
//         var length = salesDetailTable.rows().data().length;
//         if (length>0){

//             layer.msg("该井已有点位信息，不能再进行新增！！！", { icon: 0 });
//             return;
//         }
//         $scope.act = "new";
//         $scope.well_id=orgId;
//         $("#editForm")[0].reset();
//         $("#editForm").attr("action", insertUrl);
       
//         $("#myModal").modal("show");
//         z = null;
//     }
//     $scope.onQuery=function(){
//         //queryWells(orgId);
//     }
