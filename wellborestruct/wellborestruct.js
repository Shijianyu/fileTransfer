var selectUrl = appRoot + "/api/jsjgt/Select";
var selectCwsjUrl = appRoot + "/api/jsjgcwsj/Select";
var queryUrl = appRoot + "/api/jsjgcwsj/SelectIsDataByOrgId";

//点击节点获取单位下属单井列表信息
var orgId = "0";
var orgLevel = 0;


var table;
var  animal = {imageUrl: "" };
var app = angular.module('app', []);
app.controller('wellborestruct', function ($scope) {
    $("#treeDiv").css("height", document.body.clientHeight - 70);
    $("#treeDiv").css("height", $(window).height() - 70);

    $scope.level = 0;
    var onNodeClick = function (event, treeId, treeNode) {
        orgId = treeNode.id;
        orgLevel = treeNode.level;
       
        $scope.level = orgLevel;
      
        if(orgLevel<3){
            $("#datas_wrapper").show();
            queryWells(orgId);
        }else{
            $("#datas_wrapper").hide();				
			$.ajax({
				url:selectUrl,
				type:'get',
				async:true,
				data:{well_id:orgId},
				success:function(data){	
				  if(data.data.length > 0){
					animal.imageUrl = data.data[0].jsjgt;

				  }else{
					  animal.imageUrl = "#";
				  }
						
					$scope.animal = animal;	
					$scope.$apply();
				}
            });		
            $.get(selectCwsjUrl,{well_id:orgId},function(ret){
                console.log(ret);
                if(ret.code==1){
                    $scope.cwsj=ret.data;
                }else{
                    layer.msg(result.message, { icon: 2 });
                    $scope.cwsj=[];
                }
                $scope.$apply();
            },'json');	
        }	 
	   $scope.$apply();
    }

	

    $("#ztree").orgWellTree(onNodeClick, userOrgId);

    $scope.onQuery = function () {
        queryWells(orgId);
    }

    var queryWells = function (orgId) {        
        if (orgId && table) {
            var param = {
                "keywords": $("#keywords").val(),
                "org_id": orgId
            };
            table.api().settings()[0].ajax.data = param;
            table.api().ajax.reload();
            
        } else if (orgId) {            
            table = $("#datas").dataTable({
                "language": { "url": "../../js/plugins/dataTables/Chinese.json" },
                "dom": 'rt<"bottom"iflp<"clear">>',
                "pagingType": "full_numbers",
                "searching": false,
                "ordering": false,
                "autoWidth": false,
                "processing": true,
                "serverSide": true,
                "ajax": {
                    "dataSrc": "datas",
                    "type": "post",
                    "cache": false,
                    "url": queryUrl,
                    "data": function (d) {                        
                        return $.extend({}, d, {
                            "org_id": orgId
                        });
                    }
                },
                "aoColumnDefs": [{
                    "targets": [0],
                    "data": "well_id",
                    "render": function (data, type, row) {
                        var s = '<input type="checkbox" ';
                        if ("1" == row.rowDefault) {
                            s = s + 'disabled="disabled" ';
                        } else {
                            s = s + 'class="ids_" name="ids_" value="' + data + '" ';
                        }
                        return s + 'style="margin:0px;"/>';
                    }
                }, {
                    "targets": [1],
                    "data": "jmjh"
                }, {
                    "targets": [2],
                    "data": "gylx_name"
                },
                {
                    "targets": [3],
                    "data": "jx_name"
                },
                {
                    "targets": [4],
                    "data": "cctysj"
                }, {
                    "targets": [5],
                    "data": "sfczjssj"
                }, {
                    "targets": [6],
                    "data": "cjsj"
                },
                {
                    "targets": [7],
                    "data": "cjr"
                }, {
                    "targets": [8],
                    "data": "xgsj"
                },
                {
                    "targets": [9],
                    "data": "xgr",
                }]
            });
            console.log(table);
        }
    }

    queryWells(orgId);













//从数据库获取井身结构图
// $scope.onSelect = function () {
//         var strFullPath = window.document.location.href;
//         var strPath = window.document.location.pathname;
//         var pos = strFullPath.indexOf(strPath);
//         var prePath = strFullPath.substring(0, pos);
//         var postPath = strPath.substring(0, strPath.substr(1).indexOf('/') + 1);
//         console.log(prePath + postPath);
    

//     console.log(window.location.pathname);
//     var imgSrc="";
//     $scope.selectImgSrc="";
//             $.get(selectUrl, { well_id:"2101004" }, function (result) {
//                 console.log(result);
//                 if (0 == result.code) {
//                     layer.msg(result.message, { icon: 2 });
//                 } else {
//                     layer.msg("数据处理成功！", { icon: 1 });

//                     //有数据时，取出jsjgt
//                     if(result.data.length>0)
//                     {
//                         imgSrc =result.data[0].jsjgt;
//                     }
//                     console.log("图片链接："+imgSrc);
//                     //根目录+文件夹+文件名
//                      $scope.selectImgSrc="../../images/220101.jpg/"+"images/"+imgSrc;
//                      console.log($scope.selectImgSrc);
//                 }
//             }, 'json');
            

// }


    


    // var animal = {
    //     imgSrc:"../../images/220101.jpg",
    // };
    // $scope.animal = animal;
});



